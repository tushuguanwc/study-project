package com.study.fund.dao;

import com.study.fund.domain.FundCompany;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring-jdbc.xml"})
public class FundCompanyMapperTest {

    @Resource
    private FundCompanyMapper fundCompanyMapper;

    @Test
    public void testInsert() throws Exception {
        FundCompany company = new FundCompany();
        company.setCompanyId("11");
        company.setCompanyName("测试");
        int success = fundCompanyMapper.insert(company);
        Assert.assertEquals(1,success);
    }

    @Test
    public void testSelectByExample() throws Exception {
        FundCompany company = fundCompanyMapper.selectByPrimaryKey("11");
        Assert.assertNotNull(company);
        System.err.println(company.getCompanyName());
    }

    @Test
    public void testUpdateByPrimaryKeySelective() throws Exception {
        FundCompany company = fundCompanyMapper.selectByPrimaryKey("11");
        company.setCompanyName("华夏基金有限公司");
        int success = fundCompanyMapper.updateByPrimaryKeySelective(company);
        Assert.assertEquals(1,success);
    }
}
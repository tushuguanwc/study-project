package com.study.fund.dao;

import com.study.fund.domain.FundInfo;
import com.study.fund.domain.FundInfoExample;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;

import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring-jdbc.xml"})
public class FundInfoMapperTest {

    @Resource
    private FundInfoMapper fundInfoMapper;

    @Test
    public void testInsert() throws Exception {
        FundInfo fundInfo = new FundInfo();
        fundInfo.setFundId("000001");
        fundInfo.setFundCode("xhbvjsg");
        fundInfo.setFundName("华夏基金");
        fundInfo.setFundType("混合型");
        fundInfo.setFundNick("HUAXIAJIJIN");
        int success = fundInfoMapper.insert(fundInfo);
        Assert.assertEquals(1, success);
    }

    @Test
    public void testSelectByExample() throws Exception {
        FundInfo fundInfo = fundInfoMapper.selectByPrimaryKey("000001");
        Assert.assertNotNull(fundInfo);
        System.err.println(fundInfo.getFundName());
    }

    @Test
    public void testUpdateByPrimaryKeySelective() throws Exception {
        FundInfo fundInfo = fundInfoMapper.selectByPrimaryKey("000001");
        fundInfo.setFundCode("HXJJ");
        int success = fundInfoMapper.updateByPrimaryKeySelective(fundInfo);
        Assert.assertEquals(1, success);
    }
}
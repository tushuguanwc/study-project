package com.study.fund.dao;

import com.study.util.http.HttpUtils;
import org.junit.Test;

import java.text.DateFormat;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by meen on 2017/12/11.
 */
public class FundInfoQueryServiceTest {

//    private  static String url = "http://fund.eastmoney.com/pingzhongdata/001186.js?v=20160518155842";
    private  static String url = "http://fund.eastmoney.com/pingzhongdata/{0}.js?v={1}";

    private static String fmt = "yyyyMMddHHmmss";

    @Test
    public void testQuery() throws Exception {
        DateFormat format = new SimpleDateFormat(fmt);
        String date = format.format(new Date());
        String realUrl = MessageFormat.format(url, "001186", date);
        String ret = HttpUtils.get(realUrl);
        System.err.println(ret);

    }
}

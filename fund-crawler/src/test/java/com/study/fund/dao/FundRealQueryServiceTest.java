package com.study.fund.dao;

import com.study.util.http.HttpUtils;
import com.sun.javafx.binding.StringFormatter;
import org.junit.Test;

import java.text.MessageFormat;
import java.util.Date;

/**
 * Created by meen on 2017/12/11.
 */
public class FundRealQueryServiceTest {

//    private  static String url = "http://fundgz.1234567.com.cn/js/001186.js?rt=1463558676006";
    private  static String url = "http://fundgz.1234567.com.cn/js/{0}.js?rt={1}";

    @Test
    public void testQuery() throws Exception {
        long time = System.currentTimeMillis();
        String realUrl = MessageFormat.format(url, "001186", String.valueOf(time));
        String ret = HttpUtils.get(realUrl);
        System.err.println(ret);

    }
}

package com.study.fund.dao;

import com.study.fund.domain.FundInfo;
import com.study.fund.domain.FundInfoExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface FundInfoMapper {
    int countByExample(FundInfoExample example);

    int deleteByExample(FundInfoExample example);

    int deleteByPrimaryKey(String fundId);

    int insert(FundInfo record);

    int insertSelective(FundInfo record);

    List<FundInfo> selectByExample(FundInfoExample example);

    FundInfo selectByPrimaryKey(String fundId);

    int updateByExampleSelective(@Param("record") FundInfo record, @Param("example") FundInfoExample example);

    int updateByExample(@Param("record") FundInfo record, @Param("example") FundInfoExample example);

    int updateByPrimaryKeySelective(FundInfo record);

    int updateByPrimaryKey(FundInfo record);
}
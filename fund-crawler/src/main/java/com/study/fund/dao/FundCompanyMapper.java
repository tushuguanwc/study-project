package com.study.fund.dao;

import com.study.fund.domain.FundCompany;
import com.study.fund.domain.FundCompanyExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface FundCompanyMapper {
    int countByExample(FundCompanyExample example);

    int deleteByExample(FundCompanyExample example);

    int deleteByPrimaryKey(String companyId);

    int insert(FundCompany record);

    int insertSelective(FundCompany record);

    List<FundCompany> selectByExample(FundCompanyExample example);

    FundCompany selectByPrimaryKey(String companyId);

    int updateByExampleSelective(@Param("record") FundCompany record, @Param("example") FundCompanyExample example);

    int updateByExample(@Param("record") FundCompany record, @Param("example") FundCompanyExample example);

    int updateByPrimaryKeySelective(FundCompany record);

    int updateByPrimaryKey(FundCompany record);
}
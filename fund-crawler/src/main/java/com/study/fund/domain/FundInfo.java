package com.study.fund.domain;

public class FundInfo {
    private String fundId;

    private String fundCode;

    private String fundName;

    private String fundType;

    private String fundNick;

    public String getFundId() {
        return fundId;
    }

    public void setFundId(String fundId) {
        this.fundId = fundId;
    }

    public String getFundCode() {
        return fundCode;
    }

    public void setFundCode(String fundCode) {
        this.fundCode = fundCode;
    }

    public String getFundName() {
        return fundName;
    }

    public void setFundName(String fundName) {
        this.fundName = fundName;
    }

    public String getFundType() {
        return fundType;
    }

    public void setFundType(String fundType) {
        this.fundType = fundType;
    }

    public String getFundNick() {
        return fundNick;
    }

    public void setFundNick(String fundNick) {
        this.fundNick = fundNick;
    }
}
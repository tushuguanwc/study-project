package com.study.fund.service.impl;

import com.study.fund.dao.FundCompanyMapper;
import com.study.fund.domain.FundCompany;
import com.study.fund.service.FundCompanyService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * Created by meen on 2017/12/11.
 */
@Service
public class FundCompanyServiceImpl implements FundCompanyService {

    @Resource
    private FundCompanyMapper fundCompanyMapper;


    @Override
    public int insert(FundCompany record) {
        int success = fundCompanyMapper.insert(record);
        return success;
    }

    @Override
    public FundCompany selectByPrimaryKey(String companyId) {
        FundCompany company = fundCompanyMapper.selectByPrimaryKey(companyId);
        return company;
    }

    @Override
    public int updateInSelective(FundCompany record) {
        int success = fundCompanyMapper.updateByPrimaryKeySelective(record);
        return success;
    }

}

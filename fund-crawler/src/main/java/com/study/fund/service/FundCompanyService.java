package com.study.fund.service;

import com.study.fund.domain.FundCompany;

/**
 * Created by meen on 2017/12/11.
 */
public interface FundCompanyService {

    int insert(FundCompany record);

    FundCompany selectByPrimaryKey(String companyId);

    int updateInSelective(FundCompany record);

}

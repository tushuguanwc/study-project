package com.study.fund.service;

import com.study.fund.domain.FundCompany;
import com.study.fund.domain.FundInfo;

/**
 * Created by meen on 2017/12/11.
 */
public interface FundInfoService {

    int insert(FundInfo record);

    FundInfo selectByPrimaryKey(String fundId);

    int updateInSelective(FundInfo record);

}

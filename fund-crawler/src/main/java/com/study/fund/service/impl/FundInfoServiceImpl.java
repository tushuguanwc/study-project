package com.study.fund.service.impl;

import com.study.fund.dao.FundInfoMapper;
import com.study.fund.domain.FundInfo;
import com.study.fund.service.FundInfoService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * Created by meen on 2017/12/11.
 */
@Service
public class FundInfoServiceImpl implements FundInfoService {

    @Resource
    private FundInfoMapper fundInfoMapper;

    @Override
    public int insert(FundInfo record) {
        int success = fundInfoMapper.insert(record);
        return success;
    }

    @Override
    public FundInfo selectByPrimaryKey(String fundId) {
        FundInfo fundInfo =  fundInfoMapper.selectByPrimaryKey(fundId);
        return fundInfo;
    }

    @Override
    public int updateInSelective(FundInfo record) {
        int success = fundInfoMapper.updateByPrimaryKeySelective(record);
        return success;
    }
}

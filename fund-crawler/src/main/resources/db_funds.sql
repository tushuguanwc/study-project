CREATE TABLE `fund_company` (
  `company_id` varchar(10) NOT NULL,
  `company_name` varchar(32) NOT NULL,
  PRIMARY KEY (`company_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `fund_info` (
  `fund_id` varchar(10) NOT NULL,
  `fund_code` varchar(32) NOT NULL,
  `fund_name` varchar(64) NOT NULL,
  `fund_type` varchar(12) NOT NULL,
  `fund_nick` varchar(128) NOT NULL,
  PRIMARY KEY (`fund_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

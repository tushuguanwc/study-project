package com.study.server;

/**
 * Created by meen on 2017/12/4.
 */
public class MainStarter {

    public static void main(String[] args) {
        int port = 18080;
        new Thread(new TcpServer(port)).start();
    }
}

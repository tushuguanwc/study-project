package com.study.server.io;

/**
 * Created by meen on 2017/12/4.
 */
public interface GameInput {

    byte getByte();

    short getShort();

    int getInt();

    long getLong();

    String getString();

}

package com.study.server.io;

/**
 * Created by meen on 2017/12/4.
 */
public interface GameOutput {

    void putByte(byte b);

    void putShort(short s);

    void putInt(int i);

    void putLong(long l);

    void putString(String str);

}

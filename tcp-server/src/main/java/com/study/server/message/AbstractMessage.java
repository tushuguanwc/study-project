package com.study.server.message;

import com.study.server.io.GameInput;
import com.study.server.io.GameOutput;
import io.netty.buffer.ByteBuf;

/**
 * Created by meen on 2017/12/4.
 */
public abstract class AbstractMessage implements GameInput,GameOutput{

    private short cmdId;

    public AbstractMessage(short cmdId) {
        this.cmdId = cmdId;
    }

    public short getCmdId() {
        return cmdId;
    }

    public void setCmdId(short cmdId) {
        this.cmdId = cmdId;
    }

    public abstract int remaining();
}

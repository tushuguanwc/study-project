CREATE TABLE `ucdkey` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cdkey` varchar(32) NOT NULL,
  `type` varchar(32) NOT NULL,
  `codeType` varchar(32) NOT NULL,
  `rewardInfo` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `cdkey_idx` (`cdkey`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


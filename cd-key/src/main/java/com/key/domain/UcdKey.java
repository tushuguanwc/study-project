package com.key.domain;

public class UcdKey {
    private Integer id;

    private String cdkey;

    private String codetype;

    private String type;

    private String rewardinfo;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCdkey() {
        return cdkey;
    }

    public void setCdkey(String cdkey) {
        this.cdkey = cdkey;
    }

    public String getCodetype() {
        return codetype;
    }

    public void setCodetype(String codetype) {
        this.codetype = codetype;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getRewardinfo() {
        return rewardinfo;
    }

    public void setRewardinfo(String rewardinfo) {
        this.rewardinfo = rewardinfo;
    }
}
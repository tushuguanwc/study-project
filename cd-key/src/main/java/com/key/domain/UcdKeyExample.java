package com.key.domain;

import java.util.ArrayList;
import java.util.List;

public class UcdKeyExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public UcdKeyExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andCdkeyIsNull() {
            addCriterion("cdkey is null");
            return (Criteria) this;
        }

        public Criteria andCdkeyIsNotNull() {
            addCriterion("cdkey is not null");
            return (Criteria) this;
        }

        public Criteria andCdkeyEqualTo(String value) {
            addCriterion("cdkey =", value, "cdkey");
            return (Criteria) this;
        }

        public Criteria andCdkeyNotEqualTo(String value) {
            addCriterion("cdkey <>", value, "cdkey");
            return (Criteria) this;
        }

        public Criteria andCdkeyGreaterThan(String value) {
            addCriterion("cdkey >", value, "cdkey");
            return (Criteria) this;
        }

        public Criteria andCdkeyGreaterThanOrEqualTo(String value) {
            addCriterion("cdkey >=", value, "cdkey");
            return (Criteria) this;
        }

        public Criteria andCdkeyLessThan(String value) {
            addCriterion("cdkey <", value, "cdkey");
            return (Criteria) this;
        }

        public Criteria andCdkeyLessThanOrEqualTo(String value) {
            addCriterion("cdkey <=", value, "cdkey");
            return (Criteria) this;
        }

        public Criteria andCdkeyLike(String value) {
            addCriterion("cdkey like", value, "cdkey");
            return (Criteria) this;
        }

        public Criteria andCdkeyNotLike(String value) {
            addCriterion("cdkey not like", value, "cdkey");
            return (Criteria) this;
        }

        public Criteria andCdkeyIn(List<String> values) {
            addCriterion("cdkey in", values, "cdkey");
            return (Criteria) this;
        }

        public Criteria andCdkeyNotIn(List<String> values) {
            addCriterion("cdkey not in", values, "cdkey");
            return (Criteria) this;
        }

        public Criteria andCdkeyBetween(String value1, String value2) {
            addCriterion("cdkey between", value1, value2, "cdkey");
            return (Criteria) this;
        }

        public Criteria andCdkeyNotBetween(String value1, String value2) {
            addCriterion("cdkey not between", value1, value2, "cdkey");
            return (Criteria) this;
        }

        public Criteria andCodetypeIsNull() {
            addCriterion("codeType is null");
            return (Criteria) this;
        }

        public Criteria andCodetypeIsNotNull() {
            addCriterion("codeType is not null");
            return (Criteria) this;
        }

        public Criteria andCodetypeEqualTo(String value) {
            addCriterion("codeType =", value, "codetype");
            return (Criteria) this;
        }

        public Criteria andCodetypeNotEqualTo(String value) {
            addCriterion("codeType <>", value, "codetype");
            return (Criteria) this;
        }

        public Criteria andCodetypeGreaterThan(String value) {
            addCriterion("codeType >", value, "codetype");
            return (Criteria) this;
        }

        public Criteria andCodetypeGreaterThanOrEqualTo(String value) {
            addCriterion("codeType >=", value, "codetype");
            return (Criteria) this;
        }

        public Criteria andCodetypeLessThan(String value) {
            addCriterion("codeType <", value, "codetype");
            return (Criteria) this;
        }

        public Criteria andCodetypeLessThanOrEqualTo(String value) {
            addCriterion("codeType <=", value, "codetype");
            return (Criteria) this;
        }

        public Criteria andCodetypeLike(String value) {
            addCriterion("codeType like", value, "codetype");
            return (Criteria) this;
        }

        public Criteria andCodetypeNotLike(String value) {
            addCriterion("codeType not like", value, "codetype");
            return (Criteria) this;
        }

        public Criteria andCodetypeIn(List<String> values) {
            addCriterion("codeType in", values, "codetype");
            return (Criteria) this;
        }

        public Criteria andCodetypeNotIn(List<String> values) {
            addCriterion("codeType not in", values, "codetype");
            return (Criteria) this;
        }

        public Criteria andCodetypeBetween(String value1, String value2) {
            addCriterion("codeType between", value1, value2, "codetype");
            return (Criteria) this;
        }

        public Criteria andCodetypeNotBetween(String value1, String value2) {
            addCriterion("codeType not between", value1, value2, "codetype");
            return (Criteria) this;
        }

        public Criteria andTypeIsNull() {
            addCriterion("type is null");
            return (Criteria) this;
        }

        public Criteria andTypeIsNotNull() {
            addCriterion("type is not null");
            return (Criteria) this;
        }

        public Criteria andTypeEqualTo(String value) {
            addCriterion("type =", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeNotEqualTo(String value) {
            addCriterion("type <>", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeGreaterThan(String value) {
            addCriterion("type >", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeGreaterThanOrEqualTo(String value) {
            addCriterion("type >=", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeLessThan(String value) {
            addCriterion("type <", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeLessThanOrEqualTo(String value) {
            addCriterion("type <=", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeLike(String value) {
            addCriterion("type like", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeNotLike(String value) {
            addCriterion("type not like", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeIn(List<String> values) {
            addCriterion("type in", values, "type");
            return (Criteria) this;
        }

        public Criteria andTypeNotIn(List<String> values) {
            addCriterion("type not in", values, "type");
            return (Criteria) this;
        }

        public Criteria andTypeBetween(String value1, String value2) {
            addCriterion("type between", value1, value2, "type");
            return (Criteria) this;
        }

        public Criteria andTypeNotBetween(String value1, String value2) {
            addCriterion("type not between", value1, value2, "type");
            return (Criteria) this;
        }

        public Criteria andRewardinfoIsNull() {
            addCriterion("rewardInfo is null");
            return (Criteria) this;
        }

        public Criteria andRewardinfoIsNotNull() {
            addCriterion("rewardInfo is not null");
            return (Criteria) this;
        }

        public Criteria andRewardinfoEqualTo(String value) {
            addCriterion("rewardInfo =", value, "rewardinfo");
            return (Criteria) this;
        }

        public Criteria andRewardinfoNotEqualTo(String value) {
            addCriterion("rewardInfo <>", value, "rewardinfo");
            return (Criteria) this;
        }

        public Criteria andRewardinfoGreaterThan(String value) {
            addCriterion("rewardInfo >", value, "rewardinfo");
            return (Criteria) this;
        }

        public Criteria andRewardinfoGreaterThanOrEqualTo(String value) {
            addCriterion("rewardInfo >=", value, "rewardinfo");
            return (Criteria) this;
        }

        public Criteria andRewardinfoLessThan(String value) {
            addCriterion("rewardInfo <", value, "rewardinfo");
            return (Criteria) this;
        }

        public Criteria andRewardinfoLessThanOrEqualTo(String value) {
            addCriterion("rewardInfo <=", value, "rewardinfo");
            return (Criteria) this;
        }

        public Criteria andRewardinfoLike(String value) {
            addCriterion("rewardInfo like", value, "rewardinfo");
            return (Criteria) this;
        }

        public Criteria andRewardinfoNotLike(String value) {
            addCriterion("rewardInfo not like", value, "rewardinfo");
            return (Criteria) this;
        }

        public Criteria andRewardinfoIn(List<String> values) {
            addCriterion("rewardInfo in", values, "rewardinfo");
            return (Criteria) this;
        }

        public Criteria andRewardinfoNotIn(List<String> values) {
            addCriterion("rewardInfo not in", values, "rewardinfo");
            return (Criteria) this;
        }

        public Criteria andRewardinfoBetween(String value1, String value2) {
            addCriterion("rewardInfo between", value1, value2, "rewardinfo");
            return (Criteria) this;
        }

        public Criteria andRewardinfoNotBetween(String value1, String value2) {
            addCriterion("rewardInfo not between", value1, value2, "rewardinfo");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}
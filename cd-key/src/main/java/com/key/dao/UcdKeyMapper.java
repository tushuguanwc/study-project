package com.key.dao;

import com.key.domain.UcdKey;
import com.key.domain.UcdKeyExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface UcdKeyMapper {
    int countByExample(UcdKeyExample example);

    int deleteByExample(UcdKeyExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(UcdKey record);

    int insertSelective(UcdKey record);

    List<UcdKey> selectByExample(UcdKeyExample example);

    UcdKey selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") UcdKey record, @Param("example") UcdKeyExample example);

    int updateByExample(@Param("record") UcdKey record, @Param("example") UcdKeyExample example);

    int updateByPrimaryKeySelective(UcdKey record);

    int updateByPrimaryKey(UcdKey record);
}
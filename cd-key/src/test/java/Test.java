import com.key.dao.UcdKeyMapper;
import com.key.domain.UcdKey;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;
import java.util.Random;

/**
 * Created by admin on 2017/11/30.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring-jdbc.xml"})
public class Test {

    private final static String reward1 = "[{\"count\":\"20\",\"type\":0,\"itemId\":\"20401\"},{\"count\":\"10\",\"type\":0,\"itemId\":\"20400\"},{\"count\":\"3\",\"type\":0,\"itemId\":\"20511\"},{\"count\":100000,\"type\":2,\"itemId\":0},{\"count\":300,\"type\":3,\"itemId\":0}]";
    private final static String reward2 = "[{\"count\":\"40\",\"type\":0,\"itemId\":\"20401\"},{\"count\":\"30\",\"type\":0,\"itemId\":\"20400\"},{\"count\":\"3\",\"type\":0,\"itemId\":\"20512\"},{\"count\":500000,\"type\":2,\"itemId\":0},{\"count\":1000,\"type\":3,\"itemId\":0}]";
    private final static String reward3 = "[{\"count\":\"60\",\"type\":0,\"itemId\":\"20401\"},{\"count\":\"60\",\"type\":0,\"itemId\":\"20400\"},{\"count\":\"3\",\"type\":0,\"itemId\":\"20513\"},{\"count\":999999,\"type\":2,\"itemId\":0},{\"count\":2000,\"type\":3,\"itemId\":0}]";
    private final static int LEN = 8;
    private final static String characters[] = {"1", "2", "3", "4", "5", "6", "7", "8", "9", "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "m", "n", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"};


    //斗士礼包
    private final static String reward_1 = "[{\"count\":\"1\",\"type\":0,\"itemId\":\"20044\"},{\"count\":\"1\",\"type\":0,\"itemId\":\"20511\"},{\"count\":100,\"type\":3,\"itemId\":0}]";
    //勇士礼包
    private final static String reward_2 = "[{\"count\":\"2\",\"type\":0,\"itemId\":\"20044\"},{\"count\":\"1\",\"type\":0,\"itemId\":\"20512\"},{\"count\":200,\"type\":3,\"itemId\":0}]";
    //狂战礼包
    private final static String reward_3 = "[{\"count\":\"40\",\"type\":0,\"itemId\":\"20400\"},{\"count\":\"1\",\"type\":0,\"itemId\":\"20045\"},{\"count\":\"1\",\"type\":0,\"itemId\":\"20513\"},{\"count\":500,\"type\":3,\"itemId\":0}]";
    //王者礼包
    private final static String reward_4 = "[{\"count\":\"200\",\"type\":0,\"itemId\":\"20400\"},{\"count\":\"3\",\"type\":0,\"itemId\":\"20045\"},{\"count\":\"2\",\"type\":0,\"itemId\":\"20514\"},{\"count\":2000,\"type\":3,\"itemId\":0}]";


    private final static String reward_T20 = "[{\"count\":\"2\",\"type\":0,\"itemId\":\"20014\"},{\"count\":\"2\",\"type\":0,\"itemId\":\"20045\"},{\"count\":200,\"type\":3,\"itemId\":0}]";
    private final static String reward_T21 = "[{\"count\":\"20\",\"type\":0,\"itemId\":\"20400\"},{\"count\":\"5\",\"type\":0,\"itemId\":\"20014\"},{\"count\":\"3\",\"type\":0,\"itemId\":\"20045\"},{\"count\":500,\"type\":3,\"itemId\":0}]";
    private final static String reward_T22 = "[{\"count\":\"1\",\"type\":0,\"itemId\":\"20514\"},{\"count\":\"40\",\"type\":0,\"itemId\":\"20400\"},{\"count\":\"10\",\"type\":0,\"itemId\":\"20014\"},{\"count\":\"6\",\"type\":0,\"itemId\":\"20045\"},{\"count\":1000,\"type\":3,\"itemId\":0}]";
    private final static String reward_T23 = "[{\"count\":\"2\",\"type\":0,\"itemId\":\"20514\"},{\"count\":\"100\",\"type\":0,\"itemId\":\"20400\"},{\"count\":\"20\",\"type\":0,\"itemId\":\"20014\"},{\"count\":\"10\",\"type\":0,\"itemId\":\"20045\"},{\"count\":2000,\"type\":3,\"itemId\":0}]";
    private final static String reward_T24 = "[{\"count\":\"3\",\"type\":0,\"itemId\":\"20514\"},{\"count\":\"200\",\"type\":0,\"itemId\":\"20400\"},{\"count\":\"30\",\"type\":0,\"itemId\":\"20014\"},{\"count\":\"15\",\"type\":0,\"itemId\":\"20045\"},{\"count\":4000,\"type\":3,\"itemId\":0}]";
    private final static String reward_T25 = "[{\"count\":\"5\",\"type\":0,\"itemId\":\"20514\"},{\"count\":\"400\",\"type\":0,\"itemId\":\"20400\"},{\"count\":\"50\",\"type\":0,\"itemId\":\"20014\"},{\"count\":\"20\",\"type\":0,\"itemId\":\"20045\"},{\"count\":5000,\"type\":3,\"itemId\":0}]";



    @Resource
    private UcdKeyMapper ucdKeyMapper;

    @org.junit.Test
    public void testName() throws Exception {
        for(int i =0 ; i < 800;i++){
            UcdKey ucdKey = new UcdKey();
            String cdkey = generate();
            ucdKey.setCdkey(cdkey);
            ucdKey.setCodetype("01");
            ucdKey.setType("T03");//T01 青铜, T02白银, T03 黄金
            ucdKey.setRewardinfo(reward3);
            int success = ucdKeyMapper.insert(ucdKey);
            System.out.println("suc :" + success);
        }
    }

    @org.junit.Test
    public void testNewCdKey() throws Exception {
        for(int i =0 ; i < 50 ; i++) {
//        UcdKey ucdKey = new UcdKey();
            String cdkey = generate();
//        ucdKey.setCdkey(cdkey);
            String codeType = "01";
//        ucdKey.setCodetype(codeType);
            String type = "T25"; // T20:普通  T21:累计充值1000 T22:累计充值3000 T23:累计充值5000 T24:累计充值8000 T25:累计充值10000
//        ucdKey.setType(type);//T01 青铜, T02白银, T03 黄金 T11斗士礼包 T12勇士礼包 T13狂战礼包 T14王者礼包
//        ucdKey.setRewardinfo(reward_1);
            StringBuilder sb = new StringBuilder();
            sb.append("'").append(cdkey).append("'").append(",");
            sb.append("'").append(type).append("'").append(",");
            sb.append("'").append(codeType).append("'").append(",");
            sb.append("'").append(reward_T25).append("'");
            String sql = "insert into ucdkey (cdkey,type,codeType,rewardInfo) values (" + sb.toString() + ");";
//        int success = ucdKeyMapper.insert(ucdKey);
            System.out.println(sql);
//        System.out.println("suc :" + success);
        }
    }

    private static String generate() {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < LEN; i++) {
            int rand = new Random().nextInt(characters.length);
            sb.append(characters[rand]);
        }
        return sb.toString();
    }
}

-- 查询选修不少于3门课程的学生信息
SELECT *
FROM student
WHERE student.SNO
      IN (
  SELECT sc.SNO
  FROM
    sc
  GROUP BY
    sc.SNO
  HAVING
    count(*) >= 3
)
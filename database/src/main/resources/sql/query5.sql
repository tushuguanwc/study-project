-- 查询选修所有课程的学生信息
SELECT *
FROM student
WHERE NOT exists(
    SELECT *
    FROM course
    WHERE NOT EXISTS
    (
        SELECT *
        FROM sc
        WHERE
          course.cno = sc.cno
          AND
          student.sno = sc.sno
    )
)
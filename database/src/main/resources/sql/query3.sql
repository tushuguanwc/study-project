-- 查询 李强 同学没有选修的课程
SELECT *
FROM course
WHERE course.CNO NOT IN (
  SELECT sc.CNO
  FROM sc, student
  WHERE sc.SNO = student.SNO AND student.SNAME = '李强'
)
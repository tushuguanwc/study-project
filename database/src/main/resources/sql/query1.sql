-- 查询选修了程军老师全部课程的学生信息
SELECT *
FROM
  student
WHERE
  NOT exists(
      SELECT *
      FROM
        course
      WHERE
        TEACHER = '程军'
        AND
        NOT exists(
            SELECT *
            FROM
              sc
            WHERE
              sc.SNO = student.SNO
              AND
              sc.CNO = course.CNO
        )
  )
-- 查询选了王老师课程的女学生信息
SELECT *
FROM student
WHERE
  SEX = '女'
  AND student.SNO IN (
    SELECT
      DISTINCT sc.SNO
    FROM
      sc, course
    WHERE
      sc.CNO = course.CNO
      AND
      course.TEACHER LIKE '王%'
  )
-- 检索李同学没有选修的课程
SELECT *
FROM course
WHERE
  course.CNO
  NOT IN (
    SELECT DISTINCT sc.CNO
    FROM
      sc, student
    WHERE
      sc.SNO = student.SNO
      AND
      student.SNAME LIKE '李%'

  )
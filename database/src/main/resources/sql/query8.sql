-- 查询张友的各科成绩
SELECT
  student.SNAME,
  course.CNAME,
  sc.SCORE
FROM
  student, sc, course
WHERE
  sc.SNO = student.SNO
  AND
  student.SNAME = '张友'
  AND
  sc.CNO = course.CNO
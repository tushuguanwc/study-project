-- 查询选了C语言课程的学生信息和成绩,并排名

SELECT
  student.*,
  sc.SCORE
FROM sc
  RIGHT JOIN
  student
    ON
      sc.SNO = student.SNO
      AND
      sc.CNO = (
        SELECT course.CNO
        FROM course
        WHERE course.CNAME = 'C语言'
      )
ORDER BY
  sc.SCORE
DESC
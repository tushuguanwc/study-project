-- 查询C语言课程的平均分数

SELECT avg(sc.SCORE)
FROM
  sc, course
WHERE
  sc.CNO = course.CNO
  AND
  course.CNAME = 'C语言'
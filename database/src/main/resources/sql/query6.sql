-- 没有选修编译原理的学生姓名
SELECT student.SNAME
FROM student
WHERE NOT exists(
    SELECT *
    FROM course, sc
    WHERE sc.CNO = course.CNO AND course.CNAME = '编译原理' AND student.SNO = sc.SNO
)
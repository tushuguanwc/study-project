package com.study.word;

/**
 * Created by meen on 2018/1/5.
 */
public class Test {

//    private static void getMiddleOne(boolean b, Boolean boo, Boolean[] arr){
//        b = true;  //形参，不会改变原有值
//        boo = new Boolean(true);  //引用变量的直接操作相当于值传递，不会改变原来的引用变量
//        arr[0] = true;  //引用变量的属性的操作，会改变原有引用的属性，相当于传址调用
//    }
//
//    //测试
//    public static void main(String[] args) {
//        boolean b = false;
//        Boolean boo = new Boolean(false);
//        Boolean[] arr = new Boolean[]{false};
//
//        getMiddleOne(b, boo, arr);
//
//        System.out.println(b); //false
//        System.out.println(boo.toString());//false
//        System.out.println(arr[0]);//true
//
//    }


//    public static void change(Emp emp) {
//        emp.age = 50;
//        emp = new Emp();//再创建一个对象
//        emp.age = 100;
//        System.err.println(emp.age);
//    }
//
//    public static void main(String[] args) {
//        Emp emp = new Emp();
//        emp.age = 100;
//        System.out.println(emp.age);
//        change(emp);
//        System.out.println(emp.age);
//        System.out.println(emp.age);
//    }
//
//    static class Emp {
//        public int age;
//    }

    public static void methodA() throws Exception {
        try {
            throw new Exception();
        } finally {
            System.err.println("finally");
        }
    }

    public static void main(String[] args) {
        try {
            methodA();
        } catch (Exception e) {
            System.err.println("exception");
        }
        System.err.println("finished");
    }

}

package com.study.word;

import org.apdplat.word.WordSegmenter;
import org.apdplat.word.segmentation.Word;
import org.junit.Test;

import java.util.List;

/**
 * Created by meen on 2017/11/28.
 */
public class WordSegTest {

    @Test
    public void testWordSeg() throws Exception {
        List<Word> words = WordSegmenter.segWithStopWords("杨尚川是APDPlat应用级产品开发平台的作者");
        System.out.println(words);
    }
}

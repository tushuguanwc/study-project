package com.study.word;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Created by meen on 2017/11/29.
 */
@SpringBootApplication
public class MainStarter {

    public static void main(String[] args) {
        SpringApplication.run(MainStarter.class, args);
    }
}

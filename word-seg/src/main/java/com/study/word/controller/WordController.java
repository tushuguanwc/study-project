package com.study.word.controller;

import org.apdplat.word.WordSegmenter;
import org.apdplat.word.segmentation.Word;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by meen on 2017/11/29.
 */
@RestController
@EnableAutoConfiguration
public class WordController {

    @RequestMapping("/")
    String home() {
        return "Hello World!";
    }


    @RequestMapping("/word/{wordName}")
    String index(@PathVariable String wordName) {
        List<Word> words = WordSegmenter.segWithStopWords(wordName);
        return words.toString();
    }

}

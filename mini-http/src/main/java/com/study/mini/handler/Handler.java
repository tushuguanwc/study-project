package com.study.mini.handler;


import com.study.mini.http.request.HttpRequest;
import com.study.mini.http.response.HttpResponse;

import java.io.IOException;

public interface Handler {
    void serve(HttpRequest request, HttpResponse response) throws IOException;
}

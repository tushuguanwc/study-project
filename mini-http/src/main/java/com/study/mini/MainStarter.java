package com.study.mini;

import java.io.IOException;

/**
 * Created by meen on 2017/12/8.
 */
public class MainStarter {

    public static void main(String[] args) {
        MicroHTTPd server = new MicroHTTPd.Builder()
                .setPort(8089)
                .setPrefix("^/static/.*")
                .setPath("D:\\")
                .setDebug(true)
                .build();
        server.register("^/$", new IndexHttpHandler());
        try {
            server.startup();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

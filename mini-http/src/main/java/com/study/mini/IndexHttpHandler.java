package com.study.mini;

import com.study.mini.handler.HttpHandler;
import com.study.mini.http.request.HttpRequest;
import com.study.mini.http.response.HttpResponse;

import java.io.IOException;

/**
 * Created by meen on 2017/12/8.
 */
public class IndexHttpHandler extends HttpHandler {

    @Override
    public void doGet(HttpRequest req, HttpResponse resp) throws IOException {
        resp.renderText("hello\n" + req.toString());
    }

    @Override
    public void doPost(HttpRequest req, HttpResponse resp) throws IOException {
        resp.renderText("hello\n" + req.toString());
    }
}

package com.study.utils;

/**
 * Created by meen on 2018/1/2.
 */
public class ThreadJoinTest implements Runnable {

    int i;

    public ThreadJoinTest(int i) {
        this.i = i;
    }

    public void run() {
        for (int j = 0; j < 10; j++) {
            System.out.println(Thread.currentThread().getName() + "  test" + i + "  " + j);
        }
    }

    public static void main(String[] args) throws InterruptedException {
        Thread thread1 = new Thread(new ThreadJoinTest(1));
        Thread thread2 = new Thread(new ThreadJoinTest(2));
        Thread thread3 = new Thread(new ThreadJoinTest(3));
        Thread thread4 = new Thread(new ThreadJoinTest(4));
        thread1.start();
        //暂停其他线程，执行thread1线程，直到thread1线程全部执行结束。
        thread1.join();
        thread2.start();
        //暂停其他线程，执行thread2线程，直到thread1线程全部执行结束。
        thread2.join();
        thread3.start();
        //暂停其他线程，执行thread3线程，直到thread1线程全部执行结束。
        thread3.join();
        thread4.start();
        //暂停其他线程，执行thread4线程，直到thread1线程全部执行结束。
        thread4.join();
    }

}

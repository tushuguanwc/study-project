package com.study.util.http;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.ConnectException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.Charset;

/**
 * Created by meen on 2017/10/25.
 */
public class HttpUtils {

    private static Logger logger = LoggerFactory.getLogger(HttpUtils.class);


    public static String post(String urlAddress, String param) {
        HttpURLConnection uc = null;
        try {
            URL url = new URL(urlAddress);
            uc = (HttpURLConnection) url.openConnection();
            uc.setDoInput(true);
            uc.setDoOutput(true);
            uc.setInstanceFollowRedirects(true); // 不允许重定向
            uc.setRequestMethod("POST");
            uc.setConnectTimeout(45000); // 五秒连接超时
//			uc.setReadTimeout(5000); // 5秒返回超时
            uc.getOutputStream().write(param.getBytes(Charset.forName("GBK")));
            uc.getOutputStream().flush();
            Reader reader = new InputStreamReader(uc.getInputStream());
            BufferedReader bufferedReader = new BufferedReader(reader);
            String str = null;
            StringBuffer sb = new StringBuffer();
            while ((str = bufferedReader.readLine()) != null) {
                sb.append(str);
            }
            return sb.toString();

        } catch (Exception e) {
            logger.error("exception " + urlAddress, e);
            if (e instanceof ConnectException) {
                logger.error(e.getMessage());
            } else {
                logger.error(e.getMessage());
            }
        } finally {
            try {
                if (uc != null && uc.getInputStream() != null) {
                    uc.getInputStream().close();
                    uc.disconnect();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return "";
    }

    public static String get(String urlAddress) {
        BufferedReader in = null;
        try {
            URL url = new URL(urlAddress);
            HttpURLConnection uc = (HttpURLConnection) url.openConnection();
            uc.setDoInput(true);
            uc.setDoOutput(false);
            uc.setInstanceFollowRedirects(true); // 不允许重定向
            uc.setRequestMethod("GET");
            uc.setConnectTimeout(60000); // 五秒连接超时
            uc.setReadTimeout(60000); // 5秒返回超时
            uc.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            uc.connect();
            in = new BufferedReader(new InputStreamReader(uc.getInputStream(), "UTF-8"));
            String str;
            StringBuffer sb = new StringBuffer();
            while ((str = in.readLine()) != null) {
                sb.append(str);
            }
            return sb.toString();

        } catch (Exception e) {
            logger.error("exception " + urlAddress, e);
            if (e instanceof ConnectException) {
                logger.error(e.getMessage());
            } else {
                logger.error(e.getMessage());
            }
        } finally {
            try {
                if (in != null) {
                    in.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return "";
    }

}

生成2048位私钥 (PKCS1,pem格式)
openssl genrsa -out rsa_private_key.pem 2048

生成公钥(pem格式,公钥都一样不区分 PKCS1和PKCS8)
openssl rsa -in rsa_private_key.pem -pubout -out rsa_public_key.pem

PKCS1私钥转换为PKCS8(该格式一般java调用)
openssl pkcs8 -topk8 -inform PEM -in rsa_private_key.pem -outform pem -nocrypt -out pkcs8_private_key.pem

package com.study.util.file;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;

/**
 * Created by meen on 2017/12/11.
 */
public class FileUtil {

    public static String readFile(String path) {
        StringBuilder sb = new StringBuilder();
        try {
            File f = new File(path);
            FileInputStream fis = new FileInputStream(f);
            BufferedReader br = new BufferedReader(new InputStreamReader(fis));
            String readLine;
            while ((readLine = br.readLine()) != null) {
                sb.append(readLine);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return sb.toString();
    }

    public static void main(String[] args) {
        String path = "E:\\fund_names.txt";
        String files = readFile(path);
        System.err.println("file:"+files);
    }

}

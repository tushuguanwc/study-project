package com.study.httpserver.util;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URL;
import java.util.List;

/**
 * Created by meen on 2017/12/5.
 */
public class XMLUtil {
    private static Logger logger = LoggerFactory.getLogger(XMLUtil.class);
    private static SAXReader reader = new SAXReader();

    /**
     * 得到根节点
     *
     * @param: @param xmlPath
     * @param: @return
     * @return: Element
     * @Autor: Han
     */
    public static Element getRootElement(String xmlPath) {
        Document document = null;
        URL url = null;
        try {
            url = XMLUtil.class.getClassLoader().getResource(xmlPath);
            document = reader.read(url.getFile());
        } catch (DocumentException e) {
            logger.error("找不到指定的xml文件的路径" + url.getPath() + "！");
            return null;
        }
        return document.getRootElement();
    }

    /**
     * 得到该节点下的子节点集合
     *
     * @param: @param element
     * @param: @return
     * @return: List<Element>
     * @Autor: Han
     */
    @SuppressWarnings("unchecked")
    public static List<Element> getElements(Element element) {
        return element.elements();
    }

    /**
     * 得到该节点下指定的节点
     *
     * @param: @param name
     * @param: @return
     * @return: Element
     * @Autor: Han
     */
    public static Element getElement(Element element, String name) {
        Element childElement = element.element(name);
        if (childElement == null) {
            logger.error(element.getName() + "节点下没有子节点" + name);
            return null;
        }
        return childElement;
    }

    /**
     * 得到该节点的内容
     *
     * @param: @param element
     * @param: @return
     * @return: String
     * @Autor: Han
     */
    public static String getElementText(Element element) {
        return element.getText();
    }
}

package com.study.httpserver;

import com.study.httpserver.server.Server;

/**
 * Created by meen on 2017/12/5.
 */
public class MainStarter {
    //启动方法
    public static void main(String[] args) {
        new Thread(new Server(8089)).start();
    }
}

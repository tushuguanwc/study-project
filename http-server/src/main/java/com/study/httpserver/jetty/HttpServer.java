package com.study.httpserver.jetty;

import org.eclipse.jetty.server.Server;

/**
 * Created by meen on 2017/12/4.
 */
public class HttpServer {

    public static void main(String[] args) throws Exception {
        Server server = new Server(8080);
        server.setHandler(new HelloHandler());
        server.start();

        // 打印dump时的信息
        System.out.println(server.dump());

        // join当前线程
        server.join();
    }


}

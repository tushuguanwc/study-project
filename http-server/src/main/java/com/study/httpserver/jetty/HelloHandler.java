package com.study.httpserver.jetty;

import org.eclipse.jetty.server.Request;
import org.eclipse.jetty.server.handler.AbstractHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by meen on 2017/12/4.
 */
public class HelloHandler extends AbstractHandler {

    @Override
    public void handle(String s, Request request, HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws IOException, ServletException {
        // Declare response encoding and types
        httpServletResponse.setContentType("text/html; charset=utf-8");

        // Declare response status code
        httpServletResponse.setStatus(HttpServletResponse.SC_OK);
        httpServletResponse.getWriter().println("<h1>Hello World</h1>");
        httpServletResponse.getWriter().println("<h3>Hello Seven</h3>");
        request.setHandled(true);
    }
}

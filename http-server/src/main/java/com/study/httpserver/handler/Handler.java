package com.study.httpserver.handler;

import com.study.httpserver.itf.Context;

/**
 * Created by meen on 2017/12/5.
 */
public interface Handler {
    /**
     * handler service(service应该不是这样做的... - -!)
     * @param:  @param context
     * @return: void
     * @Autor: Han
     */
    public void service(Context context);

    /**
     * Get形式执行该方法
     * @param:  @param context
     * @return: void
     * @Autor: Han
     */
    public void doGet(Context context);

    /**
     * POST形式执行该方法
     * @param:  @param context
     * @return: void
     * @Autor: Han
     */
    public void doPost(Context context);

    /**
     * 销毁Handler(并没有销毁... - -!)
     * @param:  @param context
     * @return: void
     * @Autor: Han
     */
    public void destroy(Context context);
}

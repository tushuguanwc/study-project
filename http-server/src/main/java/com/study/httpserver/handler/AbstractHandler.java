package com.study.httpserver.handler;

import com.study.httpserver.itf.Context;
import com.study.httpserver.request.Request;

/**
 * Created by meen on 2017/12/5.
 */
public class AbstractHandler implements Handler {

    @Override
    public void service(Context context) {
        //通过请求方式选择具体解决方法
        String method = context.getRequest().getMethod();
        if(method.equals("GET")) {
            this.doGet(context);
        } else if (method.equals("POST")) {
            this.doPost(context);
        }
        sendResponse(context);
    }

    @Override
    public void doGet(Context context) {

    }

    @Override
    public void doPost(Context context) {

    }

    @Override
    public void destroy(Context context) {
        context = null;
    }

    /**
     * 通过上下文，返回封装response响应
     * @param:  @param context
     * @return: void
     * @Autor: Han
     */
    private void sendResponse(Context context) {
        new ResponseHandler().write(context);
    }
}

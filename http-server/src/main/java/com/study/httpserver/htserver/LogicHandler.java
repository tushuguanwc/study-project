package com.study.httpserver.htserver;

import com.study.httpserver.request.HttpRequest;
import com.study.httpserver.response.HttpResponse;

/**
 * Created by meen on 2017/12/7.
 */
public interface LogicHandler {

    String getPath();

    void doAction(HttpRequest request,HttpResponse response);

}

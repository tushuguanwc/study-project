package com.study.httpserver.htserver;

import com.study.httpserver.request.HttpRequest;
import com.study.httpserver.response.HttpResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

/**
 * Created by meen on 2017/12/7.
 */
public class LoginHandler extends AbstractLogicHandler{

    public LoginHandler() {
        super("login");
    }

    private static Logger logger = LoggerFactory.getLogger(LoginHandler.class);


    @Override
    public void doAction(HttpRequest request, HttpResponse response) {
        logger.info("login 收到请求 :"+request.toString());
        Map<String,String[]> params = request.getParams();
        for(Map.Entry<String,String[]> entry : params.entrySet()){
            System.err.println("key:"+entry.getKey() +"  value:"+entry.getValue().toString());
        }
        logger.info("login 处理请求 :"+response.toString());


    }
}

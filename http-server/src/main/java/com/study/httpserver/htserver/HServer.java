package com.study.httpserver.htserver;

import com.study.httpserver.htserver.handler.EchoGetHandler;
import com.study.httpserver.htserver.handler.EchoHeaderHandler;
import com.study.httpserver.htserver.handler.EchoPostHandler;
import com.study.httpserver.htserver.handler.RootHandler;
import com.sun.net.httpserver.HttpServer;

import java.net.InetSocketAddress;
import java.util.concurrent.Executors;

/**
 * Created by meen on 2017/12/6.
 */
public class HServer {

    public static void main(String[] args) {
        try {
            int port = 8089;
            InetSocketAddress socketAddress = new InetSocketAddress(port);
            HttpServer server = HttpServer.create(socketAddress, 0);//排队长度 默认为50

            server.createContext("/", new RootHandler());
            server.createContext("/echoHeader", new EchoHeaderHandler());
            server.createContext("/echoGet", new EchoGetHandler());
            server.createContext("/echoPost", new EchoPostHandler());

//            server.createContext("/", new Dispatcher(new HandlerMapping()));
            server.setExecutor(Executors.newCachedThreadPool());
            server.start();
            System.out.println("Server is listening on port:" + port);
        } catch (Exception e) {
            System.err.println("Server start exception:" + e.getMessage());
        }
    }
}

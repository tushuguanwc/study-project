package com.study.httpserver.htserver;

import com.study.httpserver.handler.AbstractHandler;
import com.study.httpserver.itf.Context;
import com.study.httpserver.request.HttpRequest;
import com.study.httpserver.response.HttpResponse;
import com.study.httpserver.response.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by meen on 2017/12/5.
 */
public class NotFoundHandler extends AbstractLogicHandler {

    private Logger logger = LoggerFactory.getLogger(NotFoundHandler.class);

    public NotFoundHandler() {
        super("/");
    }

    @Override
    public void doAction(HttpRequest request, HttpResponse response) {
        response.setStatuCode(404);
        response.setStatuCodeStr("Not Found");
        response.setHtmlFile("404.html");
    }

}

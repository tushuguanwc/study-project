package com.study.httpserver.htserver;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by meen on 2017/12/7.
 */
public class HandlerMapping {

    private static final Map<String, LogicHandler> handlerMap = new HashMap<String, LogicHandler>();

    public HandlerMapping() {
        init();
    }

    private void init() {
        LogicHandler loginHandler = new LoginHandler();
        handlerMap.put(loginHandler.getPath(), loginHandler);

        LogicHandler notFoundHandler = new NotFoundHandler();
        handlerMap.put(notFoundHandler.getPath(), notFoundHandler);
    }

    public LogicHandler getHandler(String path) {
        return handlerMap.get(path);
    }

}

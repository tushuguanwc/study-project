package com.study.httpserver.htserver;


/**
 * Created by meen on 2017/12/7.
 */
public abstract class AbstractLogicHandler implements LogicHandler{

    private String path;

    public AbstractLogicHandler(String path) {
        this.path = path;

    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}

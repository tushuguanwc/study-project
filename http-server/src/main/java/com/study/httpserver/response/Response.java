package com.study.httpserver.response;

import com.study.httpserver.util.XMLUtil;

import java.io.OutputStream;
import java.nio.channels.SelectionKey;

/**
 * Created by meen on 2017/12/5.
 */
public interface Response {

    //服务器名字
    public static final String SERVER_NAME = XMLUtil.getRootElement("server.xml").element("serverName").getText();

    public String getContentType();

    public int getStatuCode();

    public String getStatuCodeStr();

    public String getHtmlFile();

    public void setHtmlFile(String htmlFile);

    public void setOutputStream(OutputStream outputStream);

    public void setContentType(String contentType);

    public void setStatuCode(int statuCode);

    public void setStatuCodeStr(String statuCodeStr);

}

package com.study.httpserver.response;

import com.study.httpserver.response.Response;

import java.io.OutputStream;
import java.nio.channels.SelectionKey;

/**
 * Created by meen on 2017/12/5.
 */
public class HttpResponse implements Response {

    //内容类型  defalut 为text/html
    private String contentType = "text/html";
    //响应码  defalut 为200
    private int StatuCode = 200;
    private String statuCodeStr = "OK";
    private String htmlFile = "";

    private OutputStream content;
    private long length;

    public HttpResponse() {

    }

    @Override
    public String getContentType() {
        return contentType;
    }

    @Override
    public int getStatuCode() {
        return StatuCode;
    }

    @Override
    public String getStatuCodeStr() {
        return statuCodeStr;
    }

    @Override
    public String getHtmlFile() {
        return htmlFile;
    }

    @Override
    public void setHtmlFile(String htmlFile) {
        this.htmlFile = htmlFile;
    }

    @Override
    public void setOutputStream(OutputStream outputStream) {
        this.content = outputStream;
    }

    @Override
    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    @Override
    public void setStatuCode(int statuCode) {
        StatuCode = statuCode;
    }

    @Override
    public void setStatuCodeStr(String statuCodeStr) {
        this.statuCodeStr = statuCodeStr;
    }
}

package com.study.httpserver.itf;

import com.study.httpserver.request.Request;
import com.study.httpserver.response.Response;

/**
 * Created by meen on 2017/12/5.
 */
public abstract class Context {

    protected Request request;
    protected Response response;

    /**
     * 得到Request
     * @param:  @return
     * @return: Request
     * @Autor: Han
     */
    public Request getRequest() {
        return request;
    }

    /**
     * 得到Response
     * @param:  @return
     * @return: Response
     * @Autor: Han
     */
    public Response getResponse() {
        return response;
    }
}
